package main

import (
	router "2gis-test/internal/application/http"
	"2gis-test/internal/config"
	"2gis-test/internal/domain/order"
	order2 "2gis-test/internal/infrastructure/order"
	"2gis-test/internal/infrastructure/room_availability"
	"2gis-test/internal/infrastructure/transaction"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

const (
	envLocal = "local"
	envDev   = "dev"
	envProd  = "prod"
)

func main() {
	cfg := config.MustLoad()

	l := setupLogger(cfg.Env)

	tm := transaction.NewStubManager()
	roomAvailabilityRepo := room_availability.NewRoomAvailabilityRepo()
	orderRepo := order2.NewOrderRepo()
	orderUC := order.NewOrderUC(tm, roomAvailabilityRepo, orderRepo, l)

	r := router.SetupRouter(orderUC, l)

	l.Info("starting server", slog.String("address", cfg.HTTPAddress))

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	srv := &http.Server{
		Addr:         cfg.HTTPAddress,
		Handler:      r,
		ReadTimeout:  cfg.HTTPTimeout,
		WriteTimeout: cfg.HTTPTimeout,
		IdleTimeout:  cfg.HTTPIdleTimeout,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			l.Error("failed to start server")
		}
	}()

	l.Info("server started")

	<-done
	l.Info("stopping server")
}

func setupLogger(env string) *slog.Logger {
	var log *slog.Logger

	switch env {
	case envLocal, envDev:
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
		)
	case envProd:
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}),
		)
	default:
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}),
		)
	}

	return log
}
