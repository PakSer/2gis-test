package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"log"
	"os"
	"time"
)

type Config struct {
	Env             string        `yaml:"env" env-default:"local"`
	HTTPAddress     string        `yaml:"http_address" env-default:"localhost:8080"`
	HTTPTimeout     time.Duration `yaml:"http_timeout" env-default:"4s"`
	HTTPIdleTimeout time.Duration `yaml:"http_idle_timeout" env-default:"30s"`
}

func MustLoad() *Config {
	configPath := os.Getenv("CONF_PATH")
	if configPath == "" {
		log.Fatal("CONF_PATH is not set")
	}

	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		log.Fatalf("config file does not exist: %s", configPath)
	}

	var cfg Config

	if err := cleanenv.ReadConfig(configPath, &cfg); err != nil {
		log.Fatalf("cannot read config: %s", err)
	}

	return &cfg
}
