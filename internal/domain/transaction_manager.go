package domain

import (
	"context"
)

type (
	TransactionManager interface {
		InTransaction(ctx context.Context, callbacks ...func(ctx context.Context) error) error
	}
)
