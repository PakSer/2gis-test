package order

import (
	"2gis-test/internal/domain/entities"
	"context"
	"fmt"
)

func (uc *OrderUC) CreateOrder(ctx context.Context, o entities.Order) (entities.Order, error) {
	err := uc.transactionManager.InTransaction(ctx, func(ctx context.Context) error {
		err := uc.roomAvailabilityRepo.BookHotelRoom(ctx, o.HotelID, o.RoomID, o.From, o.To)
		if err != nil {
			return fmt.Errorf("failed to book hotel room: %w", err)
		}

		err = uc.orderRepo.CreateOrder(ctx, o)
		if err != nil {
			return fmt.Errorf("failed to create order: %w", err)
		}

		return nil
	})

	if err != nil {
		return entities.Order{}, err
	}

	return o, nil
}
