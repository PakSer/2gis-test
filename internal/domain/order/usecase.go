package order

import (
	"2gis-test/internal/domain"
	"2gis-test/internal/domain/entities"
	"context"
	"log/slog"
	"time"
)

type RoomAvailabilityRepository interface {
	BookHotelRoom(ctx context.Context, hotelID string, roomID string, dateFrom time.Time, dateTo time.Time) error
}

type OrderRepository interface {
	CreateOrder(ctx context.Context, order entities.Order) error
}

type OrderUC struct {
	transactionManager   domain.TransactionManager
	roomAvailabilityRepo RoomAvailabilityRepository
	orderRepo            OrderRepository
	log                  *slog.Logger
}

func NewOrderUC(
	tm domain.TransactionManager,
	roomAvailabilityRepo RoomAvailabilityRepository,
	orderRepo OrderRepository,
	log *slog.Logger,
) *OrderUC {
	return &OrderUC{
		transactionManager:   tm,
		roomAvailabilityRepo: roomAvailabilityRepo,
		orderRepo:            orderRepo,
		log:                  log,
	}
}
