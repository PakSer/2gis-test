package room_availability

import (
	"2gis-test/internal/domain/entities"
	"context"
	"errors"
	"sync"
	"time"
)

var availability = map[string]map[string][]entities.RoomAvailability{
	"reddison": {
		"lux": []entities.RoomAvailability{
			{
				HotelID: "reddison",
				RoomID:  "lux",
				Date:    date(2024, 1, 1),
				Quota:   1,
			},
			{
				HotelID: "reddison",
				RoomID:  "lux",
				Date:    date(2024, 1, 2),
				Quota:   1,
			},
			{
				HotelID: "reddison",
				RoomID:  "lux",
				Date:    date(2024, 1, 3),
				Quota:   1,
			},
			{
				HotelID: "reddison",
				RoomID:  "lux",
				Date:    date(2024, 1, 4),
				Quota:   1,
			},
			{
				HotelID: "reddison",
				RoomID:  "lux",
				Date:    date(2024, 1, 5),
				Quota:   0,
			},
		},
	},
}

type RoomAvailabilityRepository struct {
	mu sync.Mutex
}

func NewRoomAvailabilityRepo() *RoomAvailabilityRepository {
	return &RoomAvailabilityRepository{}
}

func (r *RoomAvailabilityRepository) BookHotelRoom(ctx context.Context, hotelID string, roomID string, dateFrom time.Time, dateTo time.Time) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	hotel, ok := availability[hotelID]
	if !ok {
		return errors.New("hotel not found")
	}

	roomAvailability, ok := hotel[roomID]
	if !ok {
		return errors.New("rooms not found")
	}

	daysToBook := daysBetween(dateFrom, dateTo)
	if len(daysToBook) == 0 {
		return nil
	}

	var roomAvailabilityIdxToBook []int
	for _, dayToBook := range daysToBook {
		found := false
		for i, a := range roomAvailability {
			if a.Date.Equal(dayToBook) && a.Quota > 0 {
				roomAvailabilityIdxToBook = append(roomAvailabilityIdxToBook, i)
				found = true
				break
			}
		}

		if !found {
			return errors.New("hotel room is not available for selected dates")
		}
	}

	for _, idx := range roomAvailabilityIdxToBook {
		availability[hotelID][roomID][idx].Quota -= 1
	}

	return nil
}

func daysBetween(from time.Time, to time.Time) []time.Time {
	if from.After(to) {
		return nil
	}

	days := make([]time.Time, 0)
	for d := toDay(from); !d.After(toDay(to)); d = d.AddDate(0, 0, 1) {
		days = append(days, d)
	}

	return days
}

func toDay(timestamp time.Time) time.Time {
	return time.Date(timestamp.Year(), timestamp.Month(), timestamp.Day(), 0, 0, 0, 0, time.UTC)
}

func date(year, month, day int) time.Time {
	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
}
