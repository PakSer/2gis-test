package order

import (
	"2gis-test/internal/domain/entities"
	"context"
	"sync"
)

type OrderRepository struct {
	orders []entities.Order
	mu     sync.Mutex
}

func NewOrderRepo() *OrderRepository {
	return &OrderRepository{}
}

func (o *OrderRepository) CreateOrder(ctx context.Context, order entities.Order) error {

	o.orders = append(o.orders, order)

	return nil
}
