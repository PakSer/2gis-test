package transaction

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"log/slog"
)

const (
	txKey string = "tx"
)

type SQLManager struct {
	db  *sqlx.DB
	log *slog.Logger
}

func (m *SQLManager) With(ctx context.Context) QueryExecutor {
	if val, ok := ctx.Value(txKey).(*sqlx.Tx); ok {
		return val
	}

	return m.db
}

type QueryExecutor interface {
	SelectContext(ctx context.Context, dest interface{}, query string, args ...interface{}) error
	GetContext(ctx context.Context, dest interface{}, query string, args ...interface{}) error
	QueryRowxContext(ctx context.Context, query string, args ...interface{}) *sqlx.Row
	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
	Rebind(query string) string
	QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row
}

func (m *SQLManager) InTransaction(ctx context.Context, callbacks ...func(ctx context.Context) error) error {
	tx, err := m.db.BeginTxx(ctx, &sql.TxOptions{
		Isolation: sql.LevelReadCommitted,
	})
	if err != nil {
		return fmt.Errorf("failed to begin tx: %w", err)
	}

	defer func() {
		if p := recover(); p != nil {
			err = tx.Rollback()

			panic(p)
		}
	}()

	ctx = context.WithValue(ctx, txKey, tx)

	for _, cb := range callbacks {
		if errCb := cb(ctx); errCb != nil {
			if rollbackErr := tx.Rollback(); rollbackErr != nil {
				m.log.Error("error rolling back a transaction: ", rollbackErr)
			}

			return fmt.Errorf("exec transaction callback error: %w", errCb)
		}
	}

	return tx.Commit()
}
