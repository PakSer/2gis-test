package transaction

import (
	"context"
	"fmt"
	"sync"
)

type StubManager struct {
	mu sync.Mutex
}

func NewStubManager() *StubManager {
	return &StubManager{}
}

func (m *StubManager) InTransaction(ctx context.Context, callbacks ...func(ctx context.Context) error) error {
	m.mu.Lock()
	defer m.mu.Unlock()

	for _, cb := range callbacks {
		if err := cb(ctx); err != nil {
			return fmt.Errorf("exec transaction callback error: %w", err)
		}
	}

	return nil
}
