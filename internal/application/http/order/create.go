package order

import (
	"2gis-test/internal/domain/entities"
	resp "2gis-test/internal/utils"
	"context"
	"errors"
	"github.com/go-chi/render"
	"io"
	"log/slog"
	"net/http"
)

type Usecase interface {
	CreateOrder(ctx context.Context, o entities.Order) (entities.Order, error)
}

func CreateOrder(uc Usecase, log *slog.Logger) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var order entities.Order

		err := render.DecodeJSON(r.Body, &order)
		if errors.Is(err, io.EOF) {
			log.Error("request body is empty")

			render.JSON(w, r, resp.Error("empty request"))

			return
		}
		if err != nil {
			log.Error("failed to decode request body", slog.Attr{
				Key:   "err",
				Value: slog.StringValue(err.Error()),
			})

			render.JSON(w, r, resp.Error("failed to decode request"))

			return
		}

		res, err := uc.CreateOrder(r.Context(), order)
		if err != nil {
			log.Error("failed to create order", slog.Attr{
				Key:   "err",
				Value: slog.StringValue(err.Error()),
			})

			render.JSON(w, r, resp.Error("failed to create order"))

			return
		}

		render.JSON(w, r, res)
	}
}
